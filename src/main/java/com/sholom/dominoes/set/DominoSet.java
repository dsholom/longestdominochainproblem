package com.sholom.dominoes.set;

import com.sholom.dominoes.domino.Domino;

import java.util.*;

/**
 * Interface that represent a set of domino tiles with some basik API
 *
 * Immutability achieved through the fact that each operation
 * return new instance and this contract should be fulfilled in sub-classes
 *
 */
public interface DominoSet {

    List<Domino> tiles();

    default DominoSet shuffle(){
        List<Domino> resultDominoSet = tiles();
        Collections.shuffle(resultDominoSet, new Random(System.nanoTime()));

        return () -> new ArrayList<>(resultDominoSet);
    }

    default DominoSet randomSubset(int n) {
        Random rand = new Random(System.nanoTime());
        List<Domino> tiles = tiles();

        List<Domino> tilesSubset = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            tilesSubset.add(tiles.remove(rand.nextInt(tiles.size())));
        }

        return () -> new ArrayList<>(tilesSubset);
    }

}
