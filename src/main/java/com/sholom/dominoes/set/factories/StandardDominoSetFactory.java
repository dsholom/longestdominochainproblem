package com.sholom.dominoes.set.factories;

import com.sholom.dominoes.domino.Domino;
import com.sholom.dominoes.set.DominoSet;
import com.sholom.dominoes.set.factories.DominoSetFactory;


import java.util.ArrayList;
import java.util.List;

public class StandardDominoSetFactory implements DominoSetFactory {

    @Override
    public DominoSet getDominoSet() {
        int minDominoSpotsNum = 0;
        int maxDominoSpotsNum = 6;
        List<Domino> dominoes = new ArrayList<>(28);

        for (int i = minDominoSpotsNum; i <= maxDominoSpotsNum; i++)
            for (int j = i; j <= maxDominoSpotsNum; j++)
                dominoes.add(new Domino(i, j));


        return () -> new ArrayList<>(dominoes);
    }
}
