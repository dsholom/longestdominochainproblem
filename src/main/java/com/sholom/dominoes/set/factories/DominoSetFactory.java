package com.sholom.dominoes.set.factories;


import com.sholom.dominoes.set.DominoSet;

public interface DominoSetFactory {

    static DominoSetFactory getDominoSetFactory(DominoSetType dominoSetType){
        return dominoSetType.getFactory();
    }

    DominoSet getDominoSet();
}
