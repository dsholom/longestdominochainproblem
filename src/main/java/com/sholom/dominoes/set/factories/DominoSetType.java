package com.sholom.dominoes.set.factories;

import com.sholom.dominoes.set.factories.DominoSetFactory;
import com.sholom.dominoes.set.factories.StandardDominoSetFactory;

/**
 * Created by Dmytro on 28.12.2015.
 */
public enum DominoSetType {
    STANDARD(new StandardDominoSetFactory());

    private DominoSetFactory dominoSetFactory;

    DominoSetType(DominoSetFactory dominoSetFactory){
        this.dominoSetFactory = dominoSetFactory;
    }

    public DominoSetFactory getFactory(){
      return dominoSetFactory;
    }
}
