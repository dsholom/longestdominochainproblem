package com.sholom.dominoes;

import com.sholom.dominoes.chain.finders.BMChainFinder;
import com.sholom.dominoes.chain.finders.ChainFinder;
import com.sholom.dominoes.set.DominoSet;
import com.sholom.dominoes.set.factories.DominoSetFactory;
import com.sholom.dominoes.set.factories.DominoSetType;

import java.util.function.BiConsumer;
import java.util.function.Function;

public class Main {

    public static void main(String[] args) {
        DominoSetFactory dominoSetFactory = DominoSetFactory.getDominoSetFactory(DominoSetType.STANDARD);
        ChainFinder dominoChainFinder = new BMChainFinder();

        DominoChainProblemUIController dominoChainProblemUIController = new DominoChainProblemUIController();
        Function<DominoSet, DominoSet> dominoSubsetSelector = dominoChainProblemUIController::selectSubset;
        BiConsumer<DominoSet, ChainFinder> longestDominoChainManager = (dominoSet, chainFinder) -> {};

        longestDominoChainManager
                .andThen(dominoChainProblemUIController::showMenuOptions)
                .andThen(dominoChainProblemUIController::selectAndPerformOption)
                .accept(dominoSubsetSelector.apply(dominoSetFactory.getDominoSet()), dominoChainFinder);
        
    }
}
