package com.sholom.dominoes.domino;

/**
 * Represent "domino" tile(bone)
 *
 * This implementation ensure that firstEnd always less or equals than secondEnd
 */
public class Domino{

    private final int firstEnd;

    private final int secondEnd;

    public Domino(int firstVal, int secondVal){
        this.firstEnd = firstVal <= secondVal? firstVal: secondVal;
        this.secondEnd = firstVal > secondVal? firstVal: secondVal;
    }

    public int getFirstEnd(){
        return firstEnd;
    }

    public int getSecondEnd(){
        return secondEnd;
    }

    @Override
    public String toString() {
        return "(" + firstEnd + "," + secondEnd + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this)
            return true;
        if(!(obj instanceof Domino))
            return false;
        Domino dominoObj = (Domino)obj;
        return dominoObj.firstEnd == firstEnd
                && dominoObj.secondEnd == secondEnd;

    }
}
