package com.sholom.dominoes.chain;

import com.sholom.dominoes.domino.Domino;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represent chain of domino tiles
 */
public class DominoChain {

    // this class implementation depends on specific methods available in LinkedList
    // that why concrete implementation used here as reference type
    // instead of general List interface
    private LinkedList<ConnectedDomino> tilesChain;

    private int tailValue;

    //used to check if chain is full,
    //but does not limit actual size
    private int recommendedMaxSize;

    public DominoChain(){
        this.tilesChain = new LinkedList<>();
    }

    public DominoChain(DominoChain chain){
        this.tilesChain = new LinkedList<>(chain.tilesChain);
        this.tailValue = chain.tailValue;
        this.recommendedMaxSize = chain.recommendedMaxSize;
    }

    public int getTailValue(){
        return tailValue;
    }


    public void setRecommendedMaxSize(int recommendedMaxSize){
        this.recommendedMaxSize = recommendedMaxSize;
    }

    public int size(){
        return tilesChain.size();
    }

    public boolean isFull(){
        return size() >= recommendedMaxSize;
    }

    public void append(Domino domino){
        if(tilesChain.size()>0) {
            if (tailValue == domino.getFirstEnd()) {
                tilesChain.add(new ConnectedDomino(domino, false));
                tailValue = domino.getSecondEnd();
            } else if (tailValue == domino.getSecondEnd()) {
                tilesChain.add(new ConnectedDomino(domino, true));
                tailValue = domino.getFirstEnd();
            }
        } else {
            tilesChain.add(new ConnectedDomino(domino, false));
            tailValue = domino.getSecondEnd();
        }
    }

    public Domino remove(){

        if(this.tilesChain.size() > 1) {
            Domino domino = tilesChain.remove().domino;
            tailValue = tilesChain.getLast().reversed ?
                    tilesChain.getLast().domino.getFirstEnd() :
                    tilesChain.getLast().domino.getSecondEnd();
            return domino;
        } else if(this.tilesChain.size() == 1)
            return tilesChain.remove().domino;
        else
            return null;
    }

    public List<Domino> tiles(){
        return tilesChain.stream()
                .map(connectedDomino -> connectedDomino.domino)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public String toString() {
        return tilesChain.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DominoChain that = (DominoChain) o;

        return tilesChain.equals(that.tilesChain);

    }

    @Override
    public int hashCode() {
        return tilesChain.hashCode();
    }

    /**
     * Wrapper for domino class that represent domino linked in chain
     * and has reversed field which shows how it directed in chain
     * (by default first end is a least one)
     *
     */
    private class ConnectedDomino{
        private final Domino domino;

        private final boolean reversed;

        public ConnectedDomino(Domino domino, boolean reversed) {
            this.domino = domino;
            this.reversed = reversed;
        }

        @Override
        public String toString() {
            return reversed?
                    "(" + domino.getSecondEnd() + "," + domino.getFirstEnd() + ")":
                    domino.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ConnectedDomino that = (ConnectedDomino) o;

            if (reversed != that.reversed) return false;
            return domino.equals(that.domino);

        }

        @Override
        public int hashCode() {
            int result = domino.hashCode();
            result = 31 * result + (reversed ? 1 : 0);
            return result;
        }
    }
}
