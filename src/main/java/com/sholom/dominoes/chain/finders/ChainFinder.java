package com.sholom.dominoes.chain.finders;

import com.sholom.dominoes.chain.DominoChain;
import com.sholom.dominoes.set.DominoSet;

/**
 *
 * Interface represents a strategy for looking up
 * some specific chain(depends on concrete implementation) in given domino set.
 *
 */
public interface ChainFinder {

    DominoChain findChain(DominoSet dominoSet);

}
