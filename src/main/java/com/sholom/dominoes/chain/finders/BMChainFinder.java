package com.sholom.dominoes.chain.finders;

import com.sholom.dominoes.chain.DominoChain;
import com.sholom.dominoes.domino.Domino;
import com.sholom.dominoes.set.DominoSet;


/**
 *
 * Represents algorithm for looking longest
 * tile chains (return first one)  based on recursive backtracking approach
 *
 * This implementation internally uses matrix for backtracking
 */
public class BMChainFinder implements ChainFinder {

    private DominoChain longestChain;
    private int maxSize = 6;

    @Override
    public DominoChain findChain(DominoSet dominoSet) {
        Domino[][] backtrackingMatrix = new Domino[maxSize+1][maxSize+1];
        int maxPossibleSize = dominoSet.tiles().size();
        DominoChain emptyChain = new DominoChain();
        emptyChain.setRecommendedMaxSize(maxPossibleSize);
        longestChain = new DominoChain();
        longestChain.setRecommendedMaxSize(maxPossibleSize);

        dominoSet.tiles().forEach(domino -> addToMatrix(backtrackingMatrix, domino));

        dominoSet.tiles().forEach(domino -> {
            DominoChain currentChain = new DominoChain(emptyChain);

            currentChain.append(domino);
            removeFromMatrix(backtrackingMatrix, domino);

            findDominoToAppend(backtrackingMatrix, currentChain);
        });

        return longestChain;
    }

    private void findDominoToAppend(Domino[][] backtrackingMatrix, DominoChain chain){
        int i = 0;

        while (i <= maxSize){
            if(backtrackingMatrix[chain.getTailValue()][i] != null){
                Domino domino = backtrackingMatrix[chain.getTailValue()][i];
                chain.append(domino);
                removeFromMatrix(backtrackingMatrix, domino);
                findDominoToAppend(backtrackingMatrix, chain);
            }
            i++;
        }

        if(chain.size() > longestChain.size())
            longestChain = new DominoChain(chain);

        if(longestChain.isFull())
            return;

        Domino domino = chain.remove();
        addToMatrix(backtrackingMatrix, domino);
    }

    private void addToMatrix(Domino[][] backtrackingMatrix, Domino domino){
        backtrackingMatrix[domino.getFirstEnd()][domino.getSecondEnd()] = domino;
        backtrackingMatrix[domino.getSecondEnd()][domino.getFirstEnd()] = domino;
    }

    private void removeFromMatrix(Domino[][] backtrackingMatrix, Domino domino){
        backtrackingMatrix[domino.getFirstEnd()][domino.getSecondEnd()] = null;
        backtrackingMatrix[domino.getSecondEnd()][domino.getFirstEnd()] = null;
    }


}
