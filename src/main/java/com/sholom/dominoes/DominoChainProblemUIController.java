package com.sholom.dominoes;

import com.sholom.dominoes.chain.DominoChain;
import com.sholom.dominoes.chain.finders.ChainFinder;
import com.sholom.dominoes.domino.Domino;
import com.sholom.dominoes.set.DominoSet;

import java.util.*;
import java.util.function.BiConsumer;

/**
 * Controller class which contain methods
 * for interaction with user via command line interface,
 * expose functionality provided by program to end user
 */
public class DominoChainProblemUIController {

    private  final Scanner scanner = new Scanner(System.in);

    private  final Map<Integer, MenuOption> menuOptions = registerMenuOptions();

    public DominoSet selectSubset(DominoSet dominoSet){
        String initMessage = "This program intended to look up for longest possible chains of domino tiles " +
                "from standard domino set and return first one.\n" +
                "Please enter how much domino tiles should be selected:";

        int standardDominoSetSize = 28;

        System.out.println(initMessage);
        int numOfDominoTiles = scanPositiveInteger(standardDominoSetSize);

        return dominoSet.shuffle().randomSubset(numOfDominoTiles);
    }

    public  void showMenuOptions(DominoSet dominoSet, ChainFinder chainFinder){
        System.out.println("Options: ");
        System.out.println("\n********************");

        for(Map.Entry<Integer, MenuOption> entry: menuOptions.entrySet())
            System.out.println(entry.getValue().getDescription());

        System.out.println("********************");
    }

    public  void selectAndPerformOption(DominoSet dominoSet, ChainFinder chainFinder) {
        int optionsCount = menuOptions.size();

        do {
            System.out.print("\nPlease choose option: ");
            int option = scanPositiveInteger(optionsCount);
            MenuOption menuOption = menuOptions.get(option);

            if (menuOption == null)
                System.out.println("\nUnexpected option. Should be one from list (number from range 1 - " +
                        optionsCount + " expected)");
            else
                menuOption.action.accept(dominoSet, chainFinder);

        } while(true);
    }

    private  Map<Integer, MenuOption> registerMenuOptions(){
        Map<Integer, MenuOption> options = new LinkedHashMap<>();

        options.put(1, new MenuOption("1. Print selected subset", (dominoSet, chainFinder) -> {
            System.out.print("Domino tiles: ");

            System.out.println(dominoSet.tiles());
        }));
        options.put(2, new MenuOption("2. Find longest chain of tiles", (dominoSet, chainFinder) -> {
            System.out.println("Looking for longest chain ...");

            DominoChain longestChain = chainFinder.findChain(dominoSet);

            System.out.println("The longest chain (size: " + longestChain.size() +"): " + longestChain);
        }));
        options.put(3, new MenuOption("3. List out-of-chain tiles", (dominoSet, chainFinder) -> {
            System.out.println("Calculating ...");

            DominoChain longestChain = chainFinder.findChain(dominoSet);
            List<Domino> outOfChain = new ArrayList<>(dominoSet.tiles());
            outOfChain.removeAll(longestChain.tiles());

            System.out.println("Out of chain tiles:" + outOfChain);
        }));
        options.put(4, new MenuOption("4. Exit", (dominoSet, chainFinder) -> System.exit(0)));

        return options;
    }

    private  int scanPositiveInteger(int limit){
        int result = 0;
        boolean incorrectInput;
        do {
            try {
                result = Integer.parseInt(scanner.nextLine());
                if(result > limit)
                    throw new NumberFormatException();
                else
                    incorrectInput = false;
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input. Should be positive integer number <= " + limit);
                incorrectInput = true;
            }
        } while(incorrectInput);

        return result;
    }

    private static class MenuOption{
        private final String description;

        private final BiConsumer<DominoSet, ChainFinder> action;

        public MenuOption(String description, BiConsumer<DominoSet, ChainFinder> action) {
            this.description = description;
            this.action = action;
        }

        public String getDescription() {
            return description;
        }

        public BiConsumer<DominoSet, ChainFinder> getAction() {
            return action;
        }
    }

}
