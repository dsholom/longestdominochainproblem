package com.sholom.dominoes.set;

import com.sholom.dominoes.domino.Domino;
import com.sholom.dominoes.set.factories.DominoSetFactory;
import com.sholom.dominoes.set.factories.DominoSetType;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
/**
 * Created by Dmytro on 28.12.2015.
 */
@RunWith(JUnitParamsRunner.class)
public class StandardDominoSetTest {

    private DominoSet standardDominoSet;
    private final int standardDominoSetZize = 28;

    @Before
    public void setUp(){
        standardDominoSet = DominoSetFactory
                .getDominoSetFactory(DominoSetType.STANDARD).getDominoSet();
    }

    @Test
    public void shouldContainCorrectTileSet(){
        List<Domino> standardSetTiles = standardDominoSet.tiles();

        assertThat(standardDominoSet.tiles().size(), equalTo(standardDominoSetZize));
        assertThat(standardSetTiles, containsInAnyOrder(standardSetTiles()));
    }

    @Test
    public void shouldShuffleElements(){
        DominoSet shuffledDominoSet = standardDominoSet.shuffle();

        assertThat(shuffledDominoSet.tiles().size(), equalTo(standardDominoSet.tiles().size()));
        assertThat(shuffledDominoSet.tiles(), not(equalTo(standardDominoSet.tiles())));
        assertThat(shuffledDominoSet.tiles(),
                containsInAnyOrder(standardDominoSet.tiles()
                        .toArray(new Domino[standardDominoSet.tiles().size()])));
    }

    @Test
    @Parameters({"3", "8", "23"})
    public void shouldReturnRandomSubset(int num){
        DominoSet randomSubset = standardDominoSet.randomSubset(num);

        assertThat(standardDominoSet.tiles(),
                hasItems(randomSubset.tiles().toArray(new Domino[num])));
    }

    private Domino[] standardSetTiles(){
        int minDominoSpotsNum = 0, maxDominoSpotsNum = 6,
                dominoSetSize = 28, index = 0;
        Domino[] dominos = new Domino[dominoSetSize];

        for (int i = minDominoSpotsNum; i <= maxDominoSpotsNum; i++)
            for (int j = i; j <= maxDominoSpotsNum; j++)
                dominos[index++] = new Domino(i,j);

        return dominos;
    }
}
