package com.sholom.dominoes.set;

import com.sholom.dominoes.set.factories.DominoSetFactory;
import com.sholom.dominoes.set.factories.DominoSetType;
import com.sholom.dominoes.set.factories.StandardDominoSetFactory;
import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;

/**
 * Created by Dmytro on 28.12.2015.
 */
public class DominoSetFactoryTest {

    @Test
    public void shouldReturnStandardDominoSetFactory() {
        assertThat(DominoSetFactory.getDominoSetFactory(DominoSetType.STANDARD),
                instanceOf(StandardDominoSetFactory.class));
    }
}
