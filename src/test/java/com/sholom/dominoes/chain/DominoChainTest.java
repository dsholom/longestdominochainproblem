package com.sholom.dominoes.chain;

import com.sholom.dominoes.domino.Domino;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by Dmytro on 28.12.2015.
 */
public class DominoChainTest {

    private DominoChain defaultDominoChain;

    private Domino[] defaultChainableDominoes;

    private Domino[] moreChainableDominoes;

    private Domino notChainableDomino;

    @Before
    public void setUp(){
        defaultChainableDominoes = new Domino[]{
                new Domino(2,5),
                new Domino(5,6),
                new Domino(6,1)
        };
        moreChainableDominoes = new Domino[]{
                new Domino(1,3),
                new Domino(3,4)
        };
        notChainableDomino = new Domino(2,5);

        defaultDominoChain = new DominoChain();
        Arrays.stream(defaultChainableDominoes).forEach(defaultDominoChain::append);
    }

    @Test
    public void shouldCreateDominoChainWithDefaultConstructor(){
        DominoChain dominoChain = new DominoChain();

        assertThat(dominoChain.size(), is(equalTo(0)));
    }

    @Test
    public void shouldCreateDominoChainWithCopyConstructor(){
        DominoChain dominoChain = new DominoChain(defaultDominoChain);

        assertThat(dominoChain.size(), is(equalTo(defaultDominoChain.size())));
        assertThat(dominoChain, is(equalTo(defaultDominoChain)));
    }

    @Test
    public void shouldBeFull(){
        int recommendedMaxSize = 5;
        defaultDominoChain.setRecommendedMaxSize(recommendedMaxSize);
        defaultDominoChain.append(moreChainableDominoes[0]);
        defaultDominoChain.append(moreChainableDominoes[1]);

        assertTrue(defaultDominoChain.isFull());
    }

    @Test
    public void shouldNotBeFull(){
        int recommendedMaxSize = 5;
        defaultDominoChain.setRecommendedMaxSize(recommendedMaxSize);
        defaultDominoChain.append(moreChainableDominoes[0]);

        assertFalse(defaultDominoChain.isFull());
    }

    @Test
    public void shouldAppendAppropriateDominoToChain(){
        int expectedTailValue = 3;
        defaultDominoChain.append(moreChainableDominoes[0]);

        assertThat(defaultDominoChain.size(), is(equalTo(defaultChainableDominoes.length + 1)));
        assertThat(defaultDominoChain.getTailValue(), is(equalTo(expectedTailValue)));
    }

    @Test
    public void shouldNotAppendAWrongDominoToChain(){
        int expectedTailValue = 1;
        defaultDominoChain.append(notChainableDomino);

        assertThat(defaultDominoChain.size(), is(equalTo(defaultChainableDominoes.length)));
        assertThat(defaultDominoChain.getTailValue(), is(equalTo(expectedTailValue)));
    }

    @Test
    public void shouldRemoveLastElementFromChain(){
        defaultDominoChain.remove();

        assertThat(defaultDominoChain.size(), is(equalTo(defaultChainableDominoes.length - 1)));
    }

    @Test
    public void shouldReturnListRepresentationOfTilesFromChain(){
        assertThat(defaultDominoChain.tiles(), containsInAnyOrder(defaultChainableDominoes));
    }
}
