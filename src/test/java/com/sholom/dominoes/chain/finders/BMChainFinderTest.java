package com.sholom.dominoes.chain.finders;

import com.sholom.dominoes.chain.DominoChain;
import com.sholom.dominoes.domino.Domino;
import com.sholom.dominoes.set.DominoSet;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static junitparams.JUnitParamsRunner.$;

/**
 * Created by Dmytro on 28.12.2015.
 */
@RunWith(JUnitParamsRunner.class)
public class BMChainFinderTest {

    @Test
    @Parameters(method = "testDominoSets")
    public void shouldFindLongestChainProperly(DominoSet dominoSet, DominoChain expectedLongestDominoChain){
        DominoChain computedDominoChain = new BMChainFinder().findChain(dominoSet);
        
        assertThat(computedDominoChain, equalTo(expectedLongestDominoChain));
    }

    private static Object[] testDominoSets(){
        return $(
                $(dominoSet(new int[][]{{4,4}, {2,4}, {2,2}, {0,2}, {4,5}}),
                        longestDominoChain(new int[][]{{2, 0}, {2, 2}, {2, 4}, {4, 4}, {4, 5}})),
                $(dominoSet(new int[][]{{2,5}, {3,6}, {0,6}, {0,3}, {1,2}, {4,4}, {2,6}, {5,6}}),
                        longestDominoChain(new int[][]{{2,5}, {5,6}, {6,0}, {0,3}, {3,6}, {6,2}, {2,1}}))
        );
    }

    private static DominoSet dominoSet(int[][] dominoValues){
        List<Domino> dominoes = new ArrayList<>();

        Arrays.stream(dominoValues).forEach(dominoVals ->
                dominoes.add(new Domino(dominoVals[0], dominoVals[1])));

        return () -> dominoes;
    }

    private static DominoChain longestDominoChain(int[][] dominoValues){
        DominoChain chain = new DominoChain();

        Arrays.stream(dominoValues).forEach(dominoVals ->
                chain.append(new Domino(dominoVals[0], dominoVals[1])));

        return chain;
    }
}
